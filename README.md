# Portfolio

## Build
### Requirements
- Composer
- Node
- Dart Sass

### Instructions
1. Install dependencies
```
$ composer install
$ npm install
```

2. Build CSS
```
$ sass ./sass/portfolio.scss ./public/css/build/portfolio.css
```

### Alternative: Docker
Deploy to production:
```
# docker compose -f docker-compose.yml -f docker-compose.prod.yml up -d
```

Cron task for automatic certificate renewal:
```sh
# run every sunday at midnight
0 0 * * 0 /path/to/certbot-renew.sh
```

Renewal script `certbot-renew.sh`:
```sh
#!/bin/sh

docker compose \
  -f /path/to/portfolio/docker-compose.yml \
  -f /path/to/portfolio/docker-compose.prod.yml \
restart
```
