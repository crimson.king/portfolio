# syntax=docker/dockerfile:1

FROM httpd:2.4-alpine AS httpd

ARG USER_ID
ARG GROUP_ID

RUN chown -R ${USER_ID}:${GROUP_ID} /usr/local/apache2 && \
  echo "Include conf/extra/httpd.conf" >> /usr/local/apache2/conf/httpd.conf && \
  echo "Include conf/extra/vhosts/*.conf" >> /usr/local/apache2/conf/httpd.conf && \
  mkdir /usr/local/apache2/conf/extra/vhosts

COPY ./docker/httpd/httpd.conf /usr/local/apache2/conf/extra/httpd.conf
COPY ./docker/httpd/harpianet.org.conf /usr/local/apache2/conf/extra/vhosts/harpianet.org.conf

#
# httpd stage to enable the ssl module
#

FROM httpd AS httpd_ssl

COPY ./docker/httpd/certbot-ssl.conf /usr/local/apache2/conf/extra/certbot-ssl.conf
COPY ./docker/httpd/harpianet.org-ssl.conf /usr/local/apache2/conf/extra/vhosts/harpianet.org-ssl.conf
COPY ./docker/httpd/ssl-entrypoint /usr/local/bin/ssl-entrypoint

WORKDIR /certbot

RUN apk update && apk add \
  git \
  python3 \
  python3-dev \
  gcc \
  musl-dev \
  augeas-libs \
  augeas-dev \
  openssl \
  openssl-dev \
  libffi-dev \
  ca-certificates \
  && \
  git clone --branch v1.29.0 https://github.com/certbot/certbot . && \
  python3 tools/venv.py

ENV PATH="/certbot/venv/bin:${PATH}"

RUN mkdir -p /var/log/letsencrypt /etc/letsencrypt /var/lib/letsencrypt && \
  chown -R ${USER_ID}:${GROUP_ID} /var/log/letsencrypt /etc/letsencrypt /var/lib/letsencrypt && \
  chmod -R g+w /var/log/letsencrypt /etc/letsencrypt /var/lib/letsencrypt

VOLUME /etc/letsencrypt
VOLUME /var/lib/letsencrypt
VOLUME /var/log/letsencrypt

ENTRYPOINT ["ssl-entrypoint"]