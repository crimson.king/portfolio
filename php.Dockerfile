# syntax=docker/dockerfile:1

#
# start php stage (development)
#

FROM php:8.1-fpm-alpine AS php

ARG USER_ID
ARG GROUP_ID

COPY ./docker/php/zz-php.ini ${PHP_INI_DIR}/conf.d/

# Pool configuration (overrides "zz-docker.conf")
COPY ./docker/php/zz-pool.conf ${PHP_INI_DIR}/../php-fpm.d/

RUN cp --remove-destination ${PHP_INI_DIR}/php.ini-development ${PHP_INI_DIR}/php.ini && \
  mkdir /var/run/php-fpm && \
  chown -R ${USER_ID}:${GROUP_ID} /var/run/php-fpm && \
  chmod -R g+w /var/run/php-fpm

# Install additional modules and their dependencies:
# - intl (icu-dev)
# - zip (libzip-dev)
RUN apk update && apk add \
  icu-dev \
  libzip-dev \
  && \
  docker-php-ext-install \
  intl \
  zip

COPY --from=composer:2 --chown=${USER_ID}:${GROUP_ID} /usr/bin/composer /usr/bin/composer
WORKDIR /var/www/harpianet.org

#
# start php stage (production)
#

FROM php AS php_composer

COPY . /var/www/harpianet.org
WORKDIR /var/www/harpianet.org

RUN cp --remove-destination ${PHP_INI_DIR}/php.ini-production ${PHP_INI_DIR}/php.ini && \
  chown -R ${USER_ID}:${GROUP_ID} /var/www/harpianet.org

USER ${USER_ID}:${GROUP_ID}

RUN echo "APP_ENV=prod" > .env.local && \
  composer install

#
# start node stage
#

FROM node:16-alpine AS node

COPY --from=php_composer --chown=${USER_ID}:${GROUP_ID} /var/www/harpianet.org /harpianet.org
WORKDIR /harpianet.org

USER ${USER_ID}:${GROUP_ID}
RUN npm install

#
# start dart stage
#

FROM dart:stable AS sass

COPY --from=node --chown=${USER_ID}:${GROUP_ID} /harpianet.org /harpianet.org

USER ${USER_ID}:${GROUP_ID}

WORKDIR /dart-sass
RUN git clone --branch 1.54.0 https://github.com/sass/dart-sass.git . && \
  dart pub get && \
  dart ./bin/sass.dart /harpianet.org/sass/portfolio.scss /harpianet.org/public/css/build/portfolio.css

#
# resume php stage
#

FROM php AS php_final

COPY --from=sass --chown=${USER_ID}:${GROUP_ID} /harpianet.org /var/www/harpianet.org

USER 0:0

# Fix permissions
RUN chmod -R ugo-w /var/www/harpianet.org && \
  chmod -R ug+w /var/www/harpianet.org/var

VOLUME /var/www/harpianet.org