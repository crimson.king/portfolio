const Portfolio = {
  webDeveloperTitle: document.querySelector('#webDeveloperTitle')
};

Portfolio.init = function() {
  window.addEventListener('load', function(event) {
    this.startTyping();
  }.bind(this));
}

Portfolio.startTyping = async function() {
  if (!this.webDeveloperTitle) {
    return false;
  }
  
  await this.delay(500);
  
  const characters = [...this.webDeveloperTitle.textContent];
  let index = 0;
  this.webDeveloperTitle.textContent = '';
  
  while (index < characters.length) {
    await this.delay(100);
    this.webDeveloperTitle.textContent += characters[index];
    index++;
  }
}

Portfolio.delay = function(milliseconds) {
  return new Promise(resolve => setTimeout(resolve, milliseconds));
}

Portfolio.init();
