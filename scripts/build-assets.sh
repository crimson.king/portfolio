# JS build
if [ ! -d ./public/js/build ]; then
    mkdir ./public/js/build
fi

# CSS build
if [ ! -d ./public/css/build ]; then
    mkdir ./public/css/build
fi

# Bootstrap
cp ./node_modules/bootstrap/dist/js/bootstrap.min.js ./public/js/build/
cp ./node_modules/bootstrap/dist/js/bootstrap.min.js.map ./public/js/build/

# Popper
cp ./node_modules/@popperjs/core/dist/umd/popper.min.js ./public/js/build/
cp ./node_modules/@popperjs/core/dist/umd/popper.min.js.map ./public/js/build/

# Fontawesome
if [ ! -d ./public/css/build/fontawesome ]; then
    mkdir ./public/css/build/fontawesome
fi

if [ ! -d ./public/css/build/fontawesome/css ]; then
    mkdir ./public/css/build/fontawesome/css
fi

if [ -d ./public/css/build/fontawesome/webfonts ]; then
    rm -rf ./public/css/build/fontawesome/webfonts
fi

cp ./node_modules/@fortawesome/fontawesome-free/css/all.min.css ./public/css/build/fontawesome/css/
cp -r ./node_modules/@fortawesome/fontawesome-free/webfonts/ ./public/css/build/fontawesome/

