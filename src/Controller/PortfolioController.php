<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PortfolioController extends AbstractController
{
    #[Route(
        '/{_locale}',
        name: 'portfolio_main_page',
        locale: 'en',
        requirements: ['_locale' => 'en|pt']
    )]
    public function mainPage(): Response
    {
        return $this->render('main.html.twig');
    }
}